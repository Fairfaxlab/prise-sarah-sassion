
.# README #
### What is this repository for? ###
 
 This web page accompanies the manuscript titled 'IFNg-producing gastrointestinal CD8+ tissue resident memory T cells are a pathological hallmark of immune checkpoint inhibitor-associated colitis', by Sarah Sassion et al, which is under review in Nature Medicine. If you want to use any of the scripts on this page in your publication, please cite this page and related puplication. For further questions, contact the corresponding author: sarah.sasson@ndm.ox.ac.uk. 
