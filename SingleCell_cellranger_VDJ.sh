#!/bin/sh
#$ -cwd
#$ -q batchq
#$ -M nassisar 
#$ -m eas
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/Single_cell_analysis/Single_cell_10x_TCRBCT/Single_cell_profiles/RESULTS/
module add python/2.7.5
module add cellranger/3.1.0
cellranger vdj --id=Colitis_VDJ --denovo --reference=/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/Single_cell_analysis/VDJ_reference_genome/refdata-cellranger-vdj-GRCh38-alts-ensembl-3.1.0/ --fastqs=/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/Single_cell_analysis/Single_cell_10x_TCRBCT/Single_cell_profiles/190912_A00711_0064_BHKLGNDMXX/FASTQ/ --sample=719267_GX11,719267_GX12,719267_GX24,719267_GX94

tar acvf /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/Single_cell_analysis/VDJ_reference_genome/refdata-cellranger-vdj-GRCh38-alts-ensembl-3.1.0.tar.gz
