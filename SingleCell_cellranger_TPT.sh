#!/bin/sh
#$ -cwd
#$ -q batchq
#$ -M nassisar 
#$ -m eas
module add python/2.7.5
module add cellranger/3.1.0
cd /t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/Single_cell_analysis/Single_cell_10x_TCRBCT/Single_cell_profiles/RESULTS/
cellranger count --chemistry=fiveprime --id=Colitis_TPT --project=Colitis_TPT --transcriptome=/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/Single_cell_analysis/reference_genome/refdata-cellranger-GRCh38-1.2.0/ --fastqs=/t1-data/data/fairfaxlab/transcriptomics/RNAseq/eQTL/monocytes/analysis/Single_cell_analysis/Single_cell_10x_TCRBCT/Single_cell_profiles/191001_A00711_0075_AHL3KLDMXX/FASTQ/ --sample=726070_GX06,726070_GX18
