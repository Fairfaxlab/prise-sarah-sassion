
echo -e '#!/bin/sh
#$ -cwd
#$ -q batchq
#$ -M username 
#$ -m eas
module add python/2.7.5
module add cellranger
cd ~
cellranger vdj --id='~name of sample' \
--reference=~/GRCh38_vdj_ref/ \
--fastqs=~'~ name of fastq file' \
--sample='~ name of output > '~/script.sh'
